import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:dreammore/helpers/Session.dart';
import 'package:dreammore/helpers/theme.dart';
import 'package:dreammore/providers/AuthProvider.dart';
import 'package:dreammore/providers/ProductsProvider.dart';
import 'package:dreammore/providers/TrendsProvider.dart';
import 'package:dreammore/widgets/Dashboard.dart';
import 'package:dreammore/widgets/SingleCategoryDetailScreen.dart';
import 'package:dreammore/widgets/SingleProductDetailsScreen.dart';
import 'package:dreammore/widgets/SplashScreen.dart';
import 'package:dreammore/widgets/AuthScreen.dart';
import 'package:dreammore/widgets/TrendsNewsItemScreen.dart';
import 'package:dreammore/widgets/components/singleCategoryItem.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:provider/provider.dart';


void main() async  {
  WidgetsFlutterBinding.ensureInitialized();
  await GlobalConfiguration().loadFromAsset("cfg");
  Session.init();
  runApp(App());
}


class App extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MyApp();
  }

}



class MyApp extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _MyAppState();
  }

}

class _MyAppState extends State<MyApp>{


  @override
  void initState() {
    super.initState();


  }

  @override
  Widget build(BuildContext context) {

    return MultiProvider(
      providers: [
        ChangeNotifierProvider(create: (context)=>ProductProvider()),
        ChangeNotifierProvider(create: (context) => AuthProvider()),
        ChangeNotifierProvider(create: (context)=>TrendsProvider()),
        ChangeNotifierProvider(create: (context)=>AuthProvider())
      ],
      child:MaterialApp(
        theme: appTheme(),
        routes: {
          'splash': (context) => SplashScreen(),
          'login' : (context) => AuthScreen(),
          'dashboard' : (context) => DashBoard(),
          'trendsDetail':(context)=>TrendsNewsItemScreen(),
          'categoryDetail':(context)=>SingleCategoryDetailScreen(),
          'productDetails':(context)=>SingleProductDetailsScreen()
        },
        initialRoute: 'splash',

      ),
    );


  }
}


