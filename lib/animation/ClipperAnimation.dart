import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:flutter/material.dart';


class ClipperAnimation extends AnimatedWidget {

  ClipperAnimation({Key key,Animation<double>animation})
      :super(key:key,listenable:animation);

  @override
  Widget build(BuildContext context) {
      final Animation<double> animation = listenable;
      return ClipPath(
        child: Container(
          height: MediaQuery.of(context).size.height,

          decoration: BoxDecoration(
            color: primaryTopGradient,
          ),
        ),

      );
  }


}