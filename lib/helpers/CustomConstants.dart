
import 'package:flutter/material.dart';


const primaryWhite = Color(0xFFFFFFFF);
const primaryTopGradient = Color(0xFFEE7A00);
const primaryBottomGradient = Color(0xFFF46942);
const neoMorphismColor = Color(0xFFEFF3F6);
const shopItemImgBg = Color(0xFFF3F5F9);
const transparent = Colors.transparent;
const neoMorphismShadowColor = Color.fromRGBO(0, 0, 0, 0.1);
const primaryBlue = Color(0xFF2095A2);

