


import 'package:shared_preferences/shared_preferences.dart';

class Session{


  static Future<SharedPreferences> get _instance async => _prefsInstance ??= await SharedPreferences.getInstance();
  static SharedPreferences _prefsInstance;

  // call this method from iniState() function of mainApp().
  static Future<SharedPreferences> init() async {
    _prefsInstance = await _instance;
    return _prefsInstance;
  }

  static String getString(String key, [String defValue]) {
    return _prefsInstance.getString(key) ?? defValue ?? "";
  }

  static Future<bool> setString(String key, String value) async {
    var prefs = await _instance;
    return prefs?.setString(key, value) ?? Future.value(false);
  }


  static Future<bool> setInt(String key, int value) async{
    var prefs = await _instance;
    return prefs?.setInt(key, value) ?? Future.value(false);
  }

  static Future<bool> setBool(String key, bool value) async{
    var prefs = await _instance;
    return prefs?.setBool(key, value) ?? Future.value(false);
  }

  static int getInt(String key){
    return _prefsInstance.getInt(key) ?? 0 ?? 0;
  }

  static bool getBool(String key){
    return _prefsInstance.getBool(key) ?? false ?? false;
  }

  static Future<void> clearAll(){
    return _prefsInstance.clear();
  }

  static Future<void> logout() async{
     var prefs = await _instance;
     return prefs.remove("user_id");
  }





}