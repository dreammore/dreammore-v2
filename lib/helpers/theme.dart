import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';


ThemeData appTheme(){
   TextTheme _basicTextTheme(TextTheme base){
     return base.copyWith(
       headline1: base.headline.copyWith(
         fontFamily: 'ProductSans-bold',
       ),
       headline2: base.headline.copyWith(
         fontFamily: 'ProductSans-bold',
       ),
       headline3: base.headline.copyWith(
         fontFamily: 'ProductSans-bold',
       ),
       headline4: base.headline.copyWith(
         fontFamily: 'ProductSans-bold',
       ),
       headline5: base.headline.copyWith(
         fontFamily: 'ProductSans',
         fontWeight: FontWeight.bold,
       ),
       headline6: base.headline.copyWith(
         fontFamily: 'ProductSans-bold',
       ),
       caption: base.caption.copyWith(
         fontFamily: 'ProductSans',
         fontWeight: FontWeight.bold,
         fontSize: 14,
         color: Colors.black
       ),


     );
   };




   final ThemeData base  = ThemeData.light();
   return base.copyWith(
     textTheme: _basicTextTheme(base.textTheme),
      primaryColor: primaryTopGradient,
     backgroundColor: Colors.white
   );
}