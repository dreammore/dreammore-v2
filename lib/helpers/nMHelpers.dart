import 'package:flutter/material.dart';
import 'package:dreammore/helpers/CustomConstants.dart';


BoxDecoration neoBoxDecoration = BoxDecoration(
      borderRadius: BorderRadius.circular(10),
      color: neoMorphismColor,
      boxShadow: [
        BoxShadow(
            color: neoMorphismShadowColor,
            offset: Offset(10,10),
            blurRadius: 10
        )
      ]
  );

BoxDecoration neoPrimaryBtnBoxDecoration = BoxDecoration(
    borderRadius: BorderRadius.circular(10),
    color: primaryTopGradient,
    boxShadow: [
      BoxShadow(
          color: neoMorphismShadowColor,
          offset: Offset(10,10),
          blurRadius: 10
      )
    ]

);

BoxDecoration neosocialLoginBoxDecoration = BoxDecoration(
    borderRadius: BorderRadius.circular(10),
    color: neoMorphismColor,
    boxShadow: [
      BoxShadow(
          color: neoMorphismShadowColor,
          offset: Offset(10,10),
          blurRadius: 10
      )
    ]
);
BoxDecoration neoShopWhite = BoxDecoration(
    color: Colors.white,
    boxShadow: [
      BoxShadow(
          color: neoMorphismShadowColor,
          offset: Offset(10,10),
          blurRadius: 10
      )
    ]
);

BoxDecoration neoSingleButtonRed = BoxDecoration(
    color: Colors.red,
    boxShadow: [
      BoxShadow(
          color: neoMorphismShadowColor,
          offset: Offset(10,10),
          blurRadius: 10
      )
    ]
);
BoxDecoration neoCategoryShopWhite = BoxDecoration(

    color: Colors.white,
    boxShadow: [
      BoxShadow(
          color: neoMorphismShadowColor,
          offset: Offset(10,10),
          blurRadius: 10
      )
    ]
);