
import 'dart:async';
import 'dart:convert';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;

class ApiClient {
  final _apiKey = 'PASTE YOUR API KEY HERE';
  final WOOCOMMERCE_API_Key = 'PASTE YOUR API KEY HERE';
  final WOOCOMMERCE_CONSUMER_Key = 'PASTE YOUR API KEY HERE';


  
  Future<http.Response>fetchAllTrends(){
    return http.get(GlobalConfiguration().getValue("API_URL")+'/trends');
  }

  Future<http.Response>fetchSingleTrendById(int id){
    return http.get(GlobalConfiguration().getValue("API_URL")+'/trend/'+id.toString());
  }

  Future<http.Response>fetchAllShopBanner(){
    return http.get(GlobalConfiguration().getValue("API_URL")+'/api/shop/banners');
  }



  Future<http.Response>registerUser({String idToken}){
    return http.post(GlobalConfiguration().getValue("API_URL")+'/api/user/register',
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: jsonEncode(<String, String>{
        'idToken':idToken
      })
    );
  }

  Future<http.Response>getAllProducts(){
    return http.get(GlobalConfiguration().getValue("API_URL")+'/api/shop/products');
  }
  
  
  Future<http.Response>fetchAllProductsCategories(){
    return http.get(GlobalConfiguration().getValue("API_URL")+'/api/shop/categories');
  }


  Future<http.Response>fetchCategorySub(parentId){
    return http.get(GlobalConfiguration().getValue("API_URL")+'/api/shop/category/'+parentId.toString()+'/subcategories');
  }


  Future<http.Response>fetchCategoryProducts({int categoryId}){
    return http.get(GlobalConfiguration().getValue("API_URL")+'/api/shop/category/'+categoryId.toString()+'/products');
  }

  Future<http.Response>fetchSingleProductDetails({int productId}){
    return http.get(GlobalConfiguration().getValue("API_URL")+'/api/shop/product/'+productId.toString());
  }
}