

import 'dart:convert';

class Utils {



  static Map<String, dynamic> parseJwt(String token){
      if(token == null){
        return null;
      }
      final List<String> parts = token.split('.');

      if (parts.length !=3){
        return null;
      }

      final String payload = parts[1];
      final String normalized = base64Url.normalize(payload);
      final String resp = utf8.decode(base64Url.decode(normalized));

      final payLoadMap = json.decode(resp);

      if (payLoadMap is! Map<String, dynamic>){
        return null;
      }
      return payLoadMap;

  }
}