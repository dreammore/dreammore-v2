import 'dart:async';

import 'package:animate_do/animate_do.dart';
import 'package:dreammore/helpers/Session.dart';
import 'package:dreammore/providers/AuthProvider.dart';
import 'package:dreammore/widgets/AuthScreen.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:provider/provider.dart';

class SplashScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {

    return _SplashScreenState();
  }
}

class _SplashScreenState extends State<SplashScreen> {
  bool _visible = false;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  @override
  void initState() {
    super.initState();

  }

  _SplashScreenState(){
    Future.delayed(const Duration(milliseconds: 4000), () async {
      AuthProvider authProvider = Provider.of<AuthProvider>(context,listen: false);

      print("is for splashscreen ${await authProvider.checkIfLogged()}");

      if (authProvider.isAuthenticated){
          Navigator.pushNamed(context, 'dashboard');
      }else{
        Navigator.pushNamed(context, 'login');
      }
    });
  }

  @override
  Widget build(BuildContext context) {

      return Scaffold(
        key: _scaffoldKey,
          body: Container(
            height: MediaQuery.of(context).size.height,
            width: MediaQuery.of(context).size.width,
            decoration: BoxDecoration(
                gradient: LinearGradient(
                    begin: Alignment.topLeft,
                    end: Alignment.bottomRight,
                    stops: [0.1, 1],
                    colors: [primaryTopGradient, primaryBottomGradient])),
            child: Stack(
              alignment: Alignment.center,
              children: <Widget>[
                ZoomIn(
                  delay: Duration(milliseconds: 100),
                  duration: Duration(seconds: 2),
                  child: Container(
                    width: 100,
                    height: 100,
                    child: Image.asset("assets/img/logo.png"),
                  ),
                ),
                AnimatedOpacity(
              opacity: _visible ? 1.0 : 0.0,
              duration: Duration(milliseconds: 500),
              child: Container(
                width: 100,
                height: 100,
                child: Image.asset("assets/img/logo.png"),
              ),
            ),
              ],
            ),
          ));

  }
}
