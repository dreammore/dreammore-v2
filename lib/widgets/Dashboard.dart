import 'package:dreammore/helpers/ApiClient.dart';
import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:dreammore/helpers/Session.dart';
import 'package:dreammore/helpers/nMHelpers.dart';
import 'package:dreammore/providers/AuthProvider.dart';
import 'package:dreammore/providers/TrendsProvider.dart';
import 'package:dreammore/widgets/components/TrendsNewsItem.dart';
import 'package:dreammore/widgets/components/loader.dart';
import 'package:dreammore/widgets/dashboard_screen/HomeScreen.dart';
import 'package:dreammore/widgets/dashboard_screen/SearchScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:provider/provider.dart';
import 'package:sliver_fill_remaining_box_adapter/sliver_fill_remaining_box_adapter.dart';

import 'dashboard_screen/ShopScreen.dart';

class DashBoard extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _DashBoardState();
  }
}

class _DashBoardState extends State<DashBoard> with SingleTickerProviderStateMixin  {
  var scrollController = ScrollController();
  TabController tabController;
  TrendsProvider trendsProvider;
  PageController _pageController;
  int selectedItemIndex = 0 ;
  final List<Widget> _children = [
    HomeScreen(),
    ShopScreen(),
    SearchScreen()
  ];

  void initState() {
    super.initState();
    tabController = TabController(length: 3, vsync: this);
    _pageController = PageController();
  }

  @override
  Widget build(BuildContext context) {
    
    


    Color tabColor = Theme.of(context).primaryColorDark;
    var appBarHeight = 125;
    var content_height = MediaQuery.of(context).size.height - appBarHeight;
    return Scaffold(


        appBar:PreferredSize(
          preferredSize: Size.fromHeight(50.0),
          child:  new AppBar(
              actions: <Widget>[
                IconButton(
                  onPressed:(){},
                  icon: Icon(MaterialCommunityIcons.bell,color: Colors.white,),
                )
              ],
              leading: Container(),
              flexibleSpace:Container(
                  alignment: Alignment(0, -0.5),
                  decoration:  BoxDecoration(
                    image: DecorationImage(
                        image: AssetImage('assets/img/bg/repeatGrid.png'),
                        repeat: ImageRepeat.repeat
                    ),
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      SizedBox(height: 35 ,),
                      Align(
                          alignment: Alignment.center,
                          child: Image.asset("assets/img/logo_v2.png")
                      ),
                    ],
                  )
              )
          ),


        ),
        body: SizedBox.expand(
          child: PageView(
            controller: _pageController,
            onPageChanged: (index){
               setState(() {
                   selectedItemIndex = index;
               });
            },
            children: <Widget>[
              HomeScreen(),
              ShopScreen(),
              SearchScreen()
            ],
          ),
        ),
       bottomNavigationBar: Row(
         children: <Widget>[
           buildNavbarItem(AntDesign.home,0),
           buildNavbarItem(AntDesign.shoppingcart,1),
           buildNavbarItem(AntDesign.search1,2),
           buildNavbarItem(FontAwesome.bookmark_o,3),
           buildNavbarItem(AntDesign.user,4)
        ]
       ),


    );

  }


 Widget  buildNavbarItem(IconData icon,int index){
    return  GestureDetector(
      onTap: (){

        print("object");
          setState(() {
             selectedItemIndex = index;
             _pageController.animateToPage(index,
                 duration: Duration(milliseconds: 500), curve: Curves.easeOut);
          });
      },
      child: Container(
        height: 60,
        width: MediaQuery.of(context).size.width/5,
          decoration: index == selectedItemIndex
              ? BoxDecoration(
              border: Border(
                bottom: BorderSide(width: 4, color: primaryTopGradient),
              ),
              gradient: LinearGradient(colors: [
                primaryTopGradient.withOpacity(0.3),
                primaryTopGradient.withOpacity(0.015),
              ], begin: Alignment.bottomCenter, end: Alignment.topCenter)
            // color: index == _selectedItemIndex ? Colors.green : Colors.white,
          )
              : BoxDecoration(),
        child: Icon(icon),
      ),

    );
  }



  @override
  void dispose(){
    _pageController.dispose();
    super.dispose();
  }

}


