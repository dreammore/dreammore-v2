import 'dart:ui';

import 'package:animate_do/animate_do.dart';
import 'package:awesome_dialog/awesome_dialog.dart';
import 'package:dreammore/animation/ClipperAnimation.dart';
import 'package:dreammore/helpers/Session.dart';
import 'package:dreammore/models/Customer.dart';
import 'package:dreammore/providers/AuthProvider.dart';

import 'package:dreammore/widgets/components/RegisterForm.dart';
import 'package:flutter/material.dart';
import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:dreammore/helpers/nMHelpers.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:mvc_pattern/mvc_pattern.dart';
import 'package:http/http.dart' as retrofit;
import 'package:provider/provider.dart';
import 'package:path_provider/path_provider.dart';
import 'dart:io';

class AuthScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _AuthScreenState();
  }
}

class _AuthScreenState extends StateMVC<AuthScreen>
    with SingleTickerProviderStateMixin {
  final _formKey = GlobalKey<FormState>();
  final _scaffoldKey = GlobalKey<ScaffoldState>();

  AuthProvider authProvider ;


  bool isProcessLoading = false;

  @override
  void initState() {
    authProvider = new AuthProvider();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    double _sheight = MediaQuery.of(context).size.height * 0.12;
    double _height = MediaQuery.of(context).size.height;
    double _width = MediaQuery.of(context).size.width;

    return Scaffold(
      key: _scaffoldKey,
      body: Container(
        height: _height,
        width: _width,
        decoration: BoxDecoration(
            image: DecorationImage(
                image: ExactAssetImage("assets/img/bg/repeatGrid.png"),
                repeat: ImageRepeat.repeat)),
        child: Stack(
          children: <Widget>[
            Container(
              height: 658,
              child: Image(
                image: AssetImage("assets/img/bg/bgPerson.jpg"),
                fit: BoxFit.cover,
              ),
            ),
            Container(
              height: _height,
              width: _width,
              child: Image(
                image: AssetImage(
                  "assets/img/bg/gradient.png",
                ),
                fit: BoxFit.cover,
              ),
            ),
            !this.authProvider.getIfisRegisteringProcess()
                ? Container(
                margin: EdgeInsets.fromLTRB(25, 100, 25, 0),
                width: _width,
                child: ChangeNotifierProvider<AuthProvider>.value(
                     value: authProvider,
                    child:  Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    SizedBox(height: 51),
                    Image(
                      image: AssetImage("assets/img/logo_mono.png"),
                    ),
                    /*first sizedBox*/
                    SizedBox(height: 61),
                    /*login button*/
                    ButtonTheme(
                      shape: RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(3.0),
                          side: BorderSide(color: Colors.white)),
                      height: 50,
                      minWidth: 286,
                      child: FlatButton.icon(
                        onPressed: () async {
                            this.authProvider.isRegisteringProcessing(true);
                            print("object before ${this.authProvider.getIfisRegisteringProcess()}");
                          try {
                            var signInWithGoogleResponse = await authProvider.signInWithGoogle();
                            var sendToServerResponse = await authProvider.sendDataToServer(authProvider.getUserIdToken());
                            if (authProvider.getUserId()!=null){
                              print("object ${this.authProvider.getIfisRegisteringProcess()}");
                              this.authProvider.isRegisteringProcessing(false);
                              if (authProvider.getRegistrationProcessCode() == 'USER_FOUND'){
                                 showSnakbar(context, "Un compte déja lié a cet mail a été trouvé dans notre système"
                                     "Vous serez automatiquement redirigez vers la page d'acceuil");
                              }else{
                                showSnakbar(context, "Merci pour votre inscription "
                                    "Vous serez automatiquement redirigez vers la page d'acceuil");
                              }
                              this.authProvider.setLogged(true,userId: this.authProvider.getUserId());


                            }

                            Future.delayed(const Duration(milliseconds: 2000), () async {
                                Navigator.pushNamed(context, 'dashboard');
                              print("object is ${await this.authProvider.checkIfLogged()}");
                              print("object from session ${Session.getBool("isAuthenticated")}");
                            });


                          } catch (error) {
                            print("Error coming from +${error}");
                          }
                        },
                        icon: Icon(
                          MaterialCommunityIcons.google,
                          color: Colors.red,
                        ),
                        color: Colors.white,
                        label: Text(
                          "Connexion avec Google",
                          style: GoogleFonts.roboto(
                              fontWeight: FontWeight.w300),
                        ),
                      ),
                    ),
                    SizedBox(height: 15),
                    Text(
                      "Ou",
                      style: GoogleFonts.roboto(color: Colors.white),
                    ),
                    SizedBox(height: 15),
                    ButtonTheme(
                      height: 50,
                      minWidth: 286,
                      child: FlatButton.icon(
                        onPressed: () {
                          print("dsdsd");
                        },
                        icon: Icon(
                          MaterialCommunityIcons.professional_hexagon,
                          color: Colors.black,
                        ),
                        label: Text(
                          "Créer un compte professionnel",
                          style: GoogleFonts.roboto(
                              fontWeight: FontWeight.w300,
                              color: Colors.black),
                        ),
                        color: Colors.white,
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(3.0),
                            side: BorderSide(color: Colors.white)),
                      ),
                    ),
                  ],
                )
                ))
                : Container(
              alignment: Alignment.center,
              child: CircularProgressIndicator(
                valueColor:
                new AlwaysStoppedAnimation<Color>(Colors.white),
              ),
            )
          ],
        ),
      ),
    );
  }


  showSnakbar(context,text){
    final snackBar = SnackBar(
      backgroundColor: Colors.white,
      content: Text(text,style: GoogleFonts.roboto(
        color: Colors.black
      )),
      action: SnackBarAction(
        textColor: primaryTopGradient,
        label: 'Compris',
        onPressed: () {
          // Some code to undo the change.
        },
      ),
    );

    _scaffoldKey.currentState.showSnackBar(snackBar);
  }

  _write(String text) async {
    final Directory directory = await getApplicationDocumentsDirectory();
    print("object ${directory.path}");
    final File file = File('${directory.path}/my_file.txt');
    await file.writeAsString(text);
  }
}
