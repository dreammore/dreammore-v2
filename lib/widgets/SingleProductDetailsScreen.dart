

import 'package:cached_network_image/cached_network_image.dart';
import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:dreammore/helpers/nMHelpers.dart';
import 'package:dreammore/models/WPImage.dart';
import 'package:dreammore/providers/ProductsProvider.dart';
import 'package:dreammore/widgets/components/_singleCategoryScreenItem.dart';
import 'package:dreammore/widgets/components/loader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_html/html_parser.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';
import 'package:sliver_fill_remaining_box_adapter/sliver_fill_remaining_box_adapter.dart';

class SingleProductDetailsScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _SingleProductDetailsScreenDetailsState();

}

class _SingleProductDetailsScreenDetailsState extends State<SingleProductDetailsScreen>{

  var scrollController = ScrollController();
  bool _show = true;


  @override
  void initState() {
    super.initState();
    handleScroll();
  }


  @override
  void dispose() {
    scrollController.removeListener(() { });
    super.dispose();
  }
  void showFloatingPayBtn(){
    setState(() {
       _show = true;
    });
  }
  void hideFloatingPayBtn(){
    setState(() {
       _show = false;
    });
  }

  void handleScroll() async {
     scrollController.addListener(() {
         if(scrollController.position.userScrollDirection == ScrollDirection.reverse){
            hideFloatingPayBtn();
         }
         if (scrollController.position.userScrollDirection == ScrollDirection.forward){
            showFloatingPayBtn();
         }

     });
   }

  @override
  Widget build(BuildContext context) {
    final SingleProductDetailsScreenArguments args = ModalRoute
        .of(context)
        .settings
        .arguments;

    return  Container(
        decoration:  BoxDecoration(
          color: shopItemImgBg,
          image: DecorationImage(
              colorFilter: ColorFilter.mode(shopItemImgBg.withOpacity(0.4), BlendMode.dstATop),
              image: AssetImage('assets/img/bg/repeatGrid.png'),
              repeat: ImageRepeat.repeat
          ),
        ),
        height: MediaQuery.of(context).size.height,
          child: ChangeNotifierProvider<ProductProvider>(
            create: (context)=>ProductProvider(),
            child: Consumer<ProductProvider>(
                builder: (BuildContext context,ProductProvider provider,Widget child){
                  print("loadingbef  state ${provider.getLoadingStatus()}");
                  if (provider.getProduct() ==null){
                    provider.fetchSingleProductsDetails(productId: args.productId);
                    print("loading state ${provider.getProduct()}");
                    if (provider.getLoadingStatus() == 1){
                      return Scaffold(
                        body:Container(
                          decoration: BoxDecoration(
                            color: shopItemImgBg,
                            image: DecorationImage(
                                colorFilter: ColorFilter.mode(shopItemImgBg.withOpacity(0.5), BlendMode.dstATop),
                                image: AssetImage('assets/img/bg/repeatGrid.png'),
                                repeat: ImageRepeat.repeat),
                          ),
                          child: Center(
                              child:Column(
                                mainAxisAlignment: MainAxisAlignment.center,
                                children: <Widget>[
                                  Image.asset("assets/img/logo-text.png",height: 50),
                                  LoaderWidget(height: 60,asset: 'assets/lotties/loader.json',)
                                ],
                              )
                          ),
                        ) ,
                      );

                    }else{
                      return Text("ok");
                    }
                  }else{
                    WPImage productImage = provider.getProduct().images[0];
                    return Scaffold(
                      body: CustomScrollView(
                        controller: scrollController,
                        physics: ScrollPhysics(parent: PageScrollPhysics()),
                        slivers: <Widget>[
                          SliverAppBar(
                            pinned: true,
                            actions: <Widget>[
                              IconButton(
                                onPressed: () {
                                  Navigator.of(context).pop();
                                },
                                icon: Icon(
                                  Icons.share,
                                  color: Colors.black,
                                ),
                              ),
                            ],
                            leading: IconButton(
                              onPressed: () {
                                Navigator.of(context).pop();
                              },
                              icon: Icon(
                                FontAwesome.angle_left,
                                color: Colors.black,
                              ),
                            ),
                            elevation: 0,
                            backgroundColor: Colors.white,
                            expandedHeight: 100,
                            centerTitle: true,
                            flexibleSpace: FlexibleSpaceBar(
                              centerTitle: true,
                              title: Text(
                                provider.getProduct().name.toString(),
                                textAlign: TextAlign.center,
                                style: GoogleFonts.roboto(color: Colors.black,
                                    fontSize: 12),
                              ),
                            ),
                          ),
                          SliverToBoxAdapter(
                            child: Center(
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 15, right: 15, bottom: 5),
                                    child: ClipRRect(
                                      child: CachedNetworkImage(
                                          imageUrl: productImage.src,
                                          fit: BoxFit.cover,
                                          height: 250,
                                          width: MediaQuery
                                              .of(context)
                                              .size
                                              .width
                                      ),
                                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                                    ),
                                  ),

                                  /*start action*/
                                  Container(
                                    margin: EdgeInsets.only(
                                        top: 20, left: 15, right: 15),
                                    child:  Row(
                                      children: <Widget>[
                                        /*premier button de l'ar */
                                        GestureDetector(
                                          child: Container(
                                            padding: EdgeInsets.all(5),
                                            decoration: neoCategoryShopWhite,
                                            height: 60,
                                            width: 150,
                                            child: Column(
                                              children: <Widget>[
                                                SizedBox(height: 4,),
                                                Icon(FlutterIcons.enviroment_ant),
                                                SizedBox(height: 4,),
                                                Text("Voir en AR ",style: GoogleFonts.roboto(
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.w600
                                                ),),
                                              ],
                                            ),
                                          ),

                                        ),
                                        SizedBox(width: 10,),

                                        GestureDetector(
                                          child: Container(
                                            padding: EdgeInsets.all(5),
                                            decoration: neoCategoryShopWhite,
                                            height: 60,
                                            width: 150,
                                            child: Column(
                                              children: <Widget>[
                                                SizedBox(height: 4,),
                                                Icon(FlutterIcons.cart_outline_mco),
                                                SizedBox(height: 4,),
                                                Text("Ajouter au panier  ",style: GoogleFonts.roboto(
                                                    color: Colors.black,
                                                    fontWeight: FontWeight.w600
                                                ),),
                                              ],
                                            ),
                                          ),

                                        ),

                                        SizedBox(width: 10,),

                                        GestureDetector(
                                          child: Container(
                                            padding: EdgeInsets.all(5),
                                            decoration: neoSingleButtonRed,
                                            height: 60,
                                            width: 60,


                                            child: Column(
                                              mainAxisAlignment: MainAxisAlignment.center,
                                              children: <Widget>[
                                                SizedBox(height: 4,),
                                                Icon(FlutterIcons.heart_ant,color: Colors.white,size: 15,),
                                                SizedBox(height: 4,),
                                              ],
                                            ),
                                          ),

                                        ),

                                      ],
                                    )
                                  ),
                                  /*end action*/
                                  Container(
                                    color: Colors.grey,
                                    height: 0.2,
                                    margin: EdgeInsets.only(
                                        top: 20, left: 15, right: 15),
                                  ),
                                  Container(
                                      margin: EdgeInsets.only(
                                        top: 20,
                                        bottom: 10,
                                        left: 15,
                                        right: 15,
                                      ),
                                      child: provider.getProduct().description!=null? Html(
                                        defaultTextStyle: GoogleFonts.roboto(
                                            fontSize: 14,
                                            color: Colors.black,
                                            fontWeight: FontWeight.w400
                                        ),
                                        onLinkTap: (url) {
                                          //TODO when a link is in the htp open a browser
                                        },
                                        data:  provider.getProduct().description,
                                      ): Text(
                                        "Aucune description n'a encore été defini pour ce article",
                                        style: GoogleFonts.roboto(
                                            fontSize: 14,
                                            fontWeight: FontWeight.w300),
                                      )
                                  ),
                                  Container(
                                    color: Colors.grey,
                                    height: 0.2,
                                    margin: EdgeInsets.only(
                                        top: 20, left: 15, right: 15),
                                  ),
                                  Container(
                                    margin: EdgeInsets.only(
                                        left: 15, right: 15, bottom: 15, top: 15),
                                    child: Column(
                                      crossAxisAlignment: CrossAxisAlignment
                                          .start,
                                      children: <Widget>[
                                        Text("Tags"),
                                        Container(
                                          margin: EdgeInsets.only(top: 5),
                                          child: Row(
                                            children: <Widget>[
                                              Text(
                                                "Tags",
                                                style:
                                                GoogleFonts.roboto(
                                                    fontWeight: FontWeight.w200),
                                              ),
                                            ],
                                          ),
                                        )
                                      ],
                                    ),
                                  ),
                                  /*comment part*/
                                  Container(
                                    color: Colors.grey[300],
                                    child: Text("data"),
                                  ),
                                ],
                              ),
                            ),
                          ),

                        ],
                      ),
                      floatingActionButton: PayButtom(context),
                      floatingActionButtonLocation: FloatingActionButtonLocation.centerFloat,
                    );
                  }
                }
            ),
          )
      );
  }

  Widget PayButtom(BuildContext context){
     return Visibility(
        visible: _show,
         child: Align(
            alignment: Alignment.bottomCenter,
           child: InkWell(
             onTap: (){
               print("dsdsds");
             },
             child: Container(
               decoration: BoxDecoration(
                   color: Colors.red,
                 borderRadius: BorderRadius.all(Radius.circular(3))
               ),
               padding: EdgeInsets.only(left: 10,right: 10),
               height: 60,
               width: 370,
               child: Row(
                 mainAxisAlignment: MainAxisAlignment.spaceBetween,
                 children: <Widget>[
                   Column(
                     mainAxisAlignment: MainAxisAlignment.center,
                     crossAxisAlignment: CrossAxisAlignment.start,
                     children: <Widget>[
                       Text("Passez commande maintenant",
                         style: GoogleFonts.workSans(
                             fontSize: 15,
                             color: Colors.white,
                             fontWeight: FontWeight.w600
                         ),),
                       SizedBox(height: 3,),
                       Text("Continuez en remplissant les informations",
                         style: GoogleFonts.workSans(
                             fontSize: 15,
                             color: Colors.white,
                             fontWeight: FontWeight.w300
                         ),)
                     ],
                   ),
                   Icon(
                      FlutterIcons.arrow_right_bold_box_outline_mco,
                      color: Colors.white,
                   )

                 ],
               ),
             ),
           ),
         )

     );
  }

}