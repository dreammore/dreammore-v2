import 'package:cached_network_image/cached_network_image.dart';
import 'package:dreammore/helpers/ApiClient.dart';
import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:dreammore/helpers/nMHelpers.dart';
import 'package:dreammore/models/GraphQLResponse/Edge.dart';
import 'package:dreammore/providers/TrendsProvider.dart';
import 'package:dreammore/widgets/Dashboard.dart';
import 'package:dreammore/widgets/components/TrendsNewsItem.dart';
import 'package:dreammore/widgets/components/_commentItem.dart';
import 'package:dreammore/widgets/components/loader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_html/flutter_html.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:graphql_flutter/graphql_flutter.dart';
import 'package:photo_view/photo_view.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:provider/provider.dart';
import 'package:sliver_fill_remaining_box_adapter/sliver_fill_remaining_box_adapter.dart';

class TrendsNewsItemScreen extends StatefulWidget {

  final trendsId;

  const TrendsNewsItemScreen({this.trendsId});

  @override
  State<StatefulWidget> createState() {
    return _TrendsNewsItemState();
  }
}

class _TrendsNewsItemState extends State<TrendsNewsItemScreen> with SingleTickerProviderStateMixin {
  var scrollController = ScrollController();
  AnimationController _hide;

  final List<Widget> _children = [];

  @override
  void initState() {
    super.initState();
    _hide = AnimationController(vsync: this, duration: kThemeAnimationDuration);
  }

  @override
  Widget build(BuildContext context) {
    final TrendItemArguments args = ModalRoute
        .of(context)
        .settings
        .arguments;
    print("object ${args.id}");

    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body:ChangeNotifierProvider<TrendsProvider>(
        create: (context)=>TrendsProvider(),
        child: Consumer<TrendsProvider>(
           builder: (context,provider,child){
              if(provider.trend == null){

                 provider.fetchSingleTrendById(args.id);
                 return Container(
                   child: Center(
                       child:Column(
                         mainAxisAlignment: MainAxisAlignment.center,
                         children: <Widget>[
                           Image.asset("assets/img/logo-text.png",height: 50),
                           LoaderWidget(height: 50,asset: 'assets/lotties/loader.json',)
                         ],
                       )
                   ),
                 );
              }else{
               return CustomScrollView(
                  controller: scrollController,

                  physics: ScrollPhysics(parent: PageScrollPhysics()),
                  slivers: <Widget>[
                    SliverAppBar(
                      pinned: true,
                      actions: <Widget>[
                        IconButton(
                          onPressed: () {
                            Navigator.of(context).pop();
                          },
                          icon: Icon(
                            Icons.share,
                            color: Colors.black,
                          ),
                        ),
                      ],
                      leading: IconButton(
                        onPressed: () {
                          Navigator.of(context).pop();
                        },
                        icon: Icon(
                          FontAwesome.angle_left,
                          color: Colors.black,
                        ),
                      ),
                      elevation: 0,
                      backgroundColor: Colors.white,
                      expandedHeight: 120,
                      centerTitle: true,
                      flexibleSpace: FlexibleSpaceBar(
                        centerTitle: true,
                        title: Text(
                           provider.trend.postTitle.toString(),
                            textAlign: TextAlign.center,
                          style: GoogleFonts.roboto(color: Colors.black,
                              fontSize: 12),
                        ),
                      ),
                    ),
                    SliverFillRemainingBoxAdapter(
                      child: Center(
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                             Container(
                    margin: EdgeInsets.only(
                        left: 15, right: 15, bottom: 5),
                    child: ClipRRect(
                      child: CachedNetworkImage(
                          imageUrl: provider.trend.featuredImage.toString(),
                          fit: BoxFit.cover,
                          height: 220,
                          width: MediaQuery
                              .of(context)
                              .size
                              .width
                      ),
                      borderRadius: BorderRadius.all(Radius.circular(10.0)),
                    ),
                  ),
                            Container(
                              margin: EdgeInsets.only(
                                  left: 15, right: 15, top: 5),
                              child: Text(
                                "Galérie photos",
                                style: GoogleFonts.roboto(fontSize: 14),
                              ),
                            ),

                            /* Container(
                    height: 80,
                    child: gallery != null ? Container(
                      margin: EdgeInsets.only(top: 15, left: 13),
                      child: ListView.builder(
                          shrinkWrap: true,
                          scrollDirection: Axis.horizontal,
                          itemCount: gallery.length,
                          itemBuilder: (BuildContext context, int index) =>
                              Container(
                                  padding: const EdgeInsets.symmetric(
                                      horizontal: 10.0),
                                  child: GestureDetector(
                                    onTap: () {
                                      print("dsd ${index}");
                                    },
                                    child: Hero(
                                      tag: index,
                                      child: CachedNetworkImage(
                                          imageUrl: gallery[0]['mediaItemUrl'],
                                          fit: BoxFit.cover,
                                          height: 80,
                                          width: 100
                                      ),),
                                  )
                              )
                      ),
                    ) : Center(
                      child: Text("Galérie vide"),
                    ),
                  ),*/
                            Container(
                              margin: EdgeInsets.only(
                                  top: 15, left: 15, right: 15, bottom: 5),
                              child: Row(
                                crossAxisAlignment: CrossAxisAlignment
                                    .start,
                                children: <Widget>[
                                  Row(
                                    children: <Widget>[
                                      Icon(
                                        AntDesign.message1,
                                        size: 15,
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text("45")
                                    ],
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                  Row(
                                    children: <Widget>[
                                      Icon(
                                        FontAwesome.bookmark_o,
                                        size: 15,
                                        color: Colors.black,
                                      ),
                                      SizedBox(
                                        width: 5,
                                      ),
                                      Text("45")
                                    ],
                                  ),
                                  SizedBox(
                                    width: 5,
                                  ),
                                ],
                              ),
                            ),
                            Container(
                              color: Colors.grey,
                              height: 0.2,
                              margin: EdgeInsets.only(
                                  top: 20, left: 15, right: 15),
                            ),
                            Container(
                                margin: EdgeInsets.only(
                                  top: 20,
                                  bottom: 10,
                                  left: 15,
                                  right: 15,
                                ),
                                child: provider.trend.postContent!=null? Html(
                                  data:  provider.trend.postContent,
                                ): Text(

                                  "Aucun contenu n'a été defini pour ce article",
                                  style: GoogleFonts.roboto(
                                      fontSize: 14,
                                      fontWeight: FontWeight.w300),
                                )
                            ),
                            Container(
                              color: Colors.grey,
                              height: 0.2,
                              margin: EdgeInsets.only(
                                  top: 20, left: 15, right: 15),
                            ),
                            Container(
                              margin: EdgeInsets.only(
                                  left: 15, right: 15, bottom: 15, top: 15),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment
                                    .start,
                                children: <Widget>[
                                  Text("Tags"),
                                  Container(
                                    margin: EdgeInsets.only(top: 5),
                                    child: Row(
                                      children: <Widget>[
                                        Text(
                                          "Tags",
                                          style:
                                          GoogleFonts.roboto(
                                              fontWeight: FontWeight.w200),
                                        ),
                                      ],
                                    ),
                                  )
                                ],
                              ),
                            ),

                            /*comment part*/
                            Container(
                              color: Colors.grey[300],
                              child: CommentsItem(),
                            ),
                          ],
                        ),
                      ),
                    )
                  ],
               );
              }
           },
        ),
      ),
      bottomNavigationBar: new Container(
        height: 80,
        width: MediaQuery
            .of(context)
            .size
            .width,
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceAround,
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(left: 5),
              height: 40,
              width: 310,
              child: TextField(
                decoration: new InputDecoration(
                    focusedBorder: OutlineInputBorder(
                        borderRadius: BorderRadius.all(
                            Radius.circular(20.0)),
                        borderSide:
                        const BorderSide(
                            color: Colors.grey, width: 0.0)),
                    border: new OutlineInputBorder(
                      borderRadius: const BorderRadius.all(
                        const Radius.circular(20.0),
                      ),
                      borderSide:
                      const BorderSide(
                          color: Colors.grey, width: 0.0),
                    ),
                    filled: true,
                    hintStyle: new TextStyle(
                        color: Colors.grey[800]),
                    hintText: "Ajouter un commentaire",
                    fillColor: Colors.grey[200]),
              ),
            ),
            IconButton(
              icon: Icon(MaterialCommunityIcons.send),
            )
          ],
        ),
        color: Colors.white,
      ),
    );
  }}