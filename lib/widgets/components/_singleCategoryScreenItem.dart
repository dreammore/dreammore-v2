import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:dreammore/helpers/nMHelpers.dart';
import 'package:dreammore/models/Category.dart';
import 'package:dreammore/models/Product.dart';
import 'package:dreammore/providers/ProductsProvider.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class SingleCategoryItemScreen extends StatefulWidget {
  int _categoryId;

  SingleCategoryItemScreen(this._categoryId);

  @override
  State<StatefulWidget> createState() {
    return _SingleCategoryItemScreenState();
  }
}

class _SingleCategoryItemScreenState extends State<SingleCategoryItemScreen> {
  @override
  Widget build(BuildContext context) {
    print("current category ${widget._categoryId}");

    return ChangeNotifierProvider<ProductProvider>(
        create: (context) => ProductProvider(),
        child: Consumer(builder: (BuildContext context,
            ProductProvider _productProvider, Widget child) {
          if (_productProvider.getCategoriesProducts() != null) {
            if (_productProvider.getLoadingState()==false){
              _productProvider.fetchSingleCategoryProducts(categoryId: widget._categoryId);
              return Text("fetching");
            }else{
               if (_productProvider.getCategoriesProducts().length == 0){
                  return Text("product empty");
               }else{
                 return ListView.builder(
                     itemCount:
                     _productProvider.getCategoriesProducts().length,
                     itemBuilder: (BuildContext context, int index) {
                       var product = _productProvider.getCategoriesProducts()[index];
                       return GestureDetector(
                         onTap: (){
                           Navigator.pushNamed(context, 'productDetails',arguments: SingleProductDetailsScreenArguments(product.id));
                         },
                         child: Container(
                           decoration: BoxDecoration(
                               borderRadius:
                               BorderRadius.all(Radius.circular(4))),
                           margin: EdgeInsets.only(left: 10, right: 10, top: 17),
                           width:MediaQuery.of(context).size.width,
                           height: 330,
                           child: Column(
                             crossAxisAlignment: CrossAxisAlignment.start,
                             mainAxisAlignment: MainAxisAlignment.start,
                             children: <Widget>[
                               Container(
                                 height: 200,
                                 child: ListView.builder(
                                     scrollDirection: Axis.horizontal,
                                     itemCount: product.images.length,
                                     itemBuilder:
                                         (BuildContext context, int index) {
                                       var image = product.images[index];
                                       return Container(
                                           height: 200,
                                           width:MediaQuery.of(context).size.width - 49,
                                           margin: EdgeInsets.only(
                                               right: 0,
                                               top: 5,
                                               bottom: 5,
                                               left: 5),
                                           child: ClipRRect(
                                             borderRadius:
                                             BorderRadius.circular(4.0),
                                             child: Image.network(
                                               image.src,
                                               fit: BoxFit.cover,
                                               height: 200,
                                               width: 200,
                                             )
                                             ,
                                           ));
                                     }),
                               ),
                               Container(
                                 margin: EdgeInsets.only(top: 14),
                                 child: Text(
                                   product.name,
                                   style: GoogleFonts.workSans(
                                       fontSize: 14,
                                       fontWeight: FontWeight.w500),
                                 ),
                               ),
                               Container(
                                 margin: EdgeInsets.only(top: 14),
                                 child: Text(
                                   "Now Only 299",
                                   style: GoogleFonts.workSans(
                                       fontSize: 14,
                                       color: primaryTopGradient,
                                       fontWeight: FontWeight.bold),
                                 ),
                               ),
                               Container(
                                   margin: EdgeInsets.only(top: 14),
                                   child: Row(
                                     mainAxisAlignment:
                                     MainAxisAlignment.spaceBetween,
                                     children: <Widget>[
                                       Container(
                                         child: Row(
                                           children: <Widget>[
                                             Text("data"),
                                             Text(
                                               " 255 utilisateurs",
                                               style: GoogleFonts.workSans(
                                                   fontSize: 12),
                                             ),
                                           ],
                                         ),
                                       ),
                                       Container(
                                         child: Row(
                                           children: <Widget>[
                                             Container(
                                               margin:
                                               EdgeInsets.only(right: 10),
                                               child: GestureDetector(
                                                 onTap: () {
                                                   print("ds");
                                                 },
                                                 child: Container(
                                                   decoration: neoShopWhite,
                                                   height: 30,
                                                   width: 30,
                                                   child: Icon(
                                                     AntDesign.heart,
                                                     size: 15,
                                                     color: Colors.red,
                                                   ),
                                                 ),
                                               ),
                                             ),
                                             Container(
                                               margin:
                                               EdgeInsets.only(right: 10),
                                               child: GestureDetector(
                                                 onTap: () {
                                                   print("ds");
                                                 },
                                                 child: Container(
                                                   decoration: neoShopWhite,
                                                   height: 30,
                                                   width: 30,
                                                   child: Icon(
                                                     AntDesign.shoppingcart,
                                                     size: 15,
                                                     color: Colors.black,
                                                   ),
                                                 ),
                                               ),
                                             ),
                                           ],
                                         ),
                                       ),
                                     ],
                                   )),
                               Container(
                                 margin: EdgeInsets.only(top: 20),
                                 height: 0.5,
                                 color: Colors.grey,
                                 width: MediaQuery.of(context).size.width,
                               )
                             ],
                           ),
                         ),
                       );
                     });
               }
            }
          } else {
            return Text("error loading");
          }
        }));
    /**/
  }
}


class SingleProductDetailsScreenArguments {
  int _productId;

  SingleProductDetailsScreenArguments(this._productId);

  int get productId => _productId;
}
