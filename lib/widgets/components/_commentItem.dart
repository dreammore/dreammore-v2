
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class CommentsItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
      return Container(
        padding: EdgeInsets.all(15),
        margin: EdgeInsets.only(left: 15,right: 15,bottom: 5),
         child: Row(
           mainAxisAlignment: MainAxisAlignment.spaceBetween,
           children: <Widget>[
                CircleAvatar(
                  backgroundImage: AssetImage('assets/icons/ic_avatar.png'),
                ),
              Container(
                margin: EdgeInsets.only(left: 10,right: 10),
                child:  Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text("Emile Zola",style: GoogleFonts.roboto(
                        fontWeight: FontWeight.w300
                    ),),
                    Container(
                      margin: EdgeInsets.only(top: 5),
                      child: Text("lorem ipsum dolor sit emet"),
                    )
                  ],
                )
              ),
              IconButton(
                onPressed: (){

                },
                icon: Icon(AntDesign.heart,size: 15,color: Colors.red,),
              )
           ],
         ),
      );
  }


}