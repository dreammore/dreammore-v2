import 'package:dreammore/helpers/nMHelpers.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:google_fonts/google_fonts.dart';

class TrendingNews extends StatefulWidget{

  final dynamic postData;

  TrendingNews({this.postData});

  @override
  State<StatefulWidget> createState() {
    // TODO: implement createState
    return _TrendingsNewsState();
  }

}


class _TrendingsNewsState extends State<TrendingNews>{




  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    var constraints = new BoxConstraints();

    var _constraintsheight = constraints.maxHeight * .65;

    
    return Stack(
        children: <Widget>[
          ClipRRect(
            child: Image.network(
              widget.postData['node']['featuredImage']['node']['mediaItemUrl'],
              fit:BoxFit.cover,
              height: 200,
              width: MediaQuery.of(context).size.width * 0.9,
            ),
            borderRadius: BorderRadius.all(Radius.circular(10.0)),
          ),
          GestureDetector(
            onTap: () async{
                print("object");
            },
            child: Container(
              width: size.width * 0.9,
              height: 192.0,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.all(
                  Radius.circular(8.0)
                ),
                gradient: LinearGradient(
                  colors: [
                    Colors.black.withOpacity(0.8),
                    Colors.black.withOpacity(0.0),
                  ],
                  begin: Alignment.topCenter,
                  end: Alignment.bottomCenter,
                  stops: [
                    0.0,
                    0.7
                  ]
                )
              ),
            ),
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(
                  left: 12.0,
                  top: 12.0,
                  right: 20.0
                ),
                child: Text(
                  widget.postData['node']['title'],
                  style: GoogleFonts.poppins(
                    color: Colors.white
                  ),
                  overflow: TextOverflow.ellipsis,
                  maxLines: 3,
                ),
              )
            ],
          )
        ],
    );
  }

}