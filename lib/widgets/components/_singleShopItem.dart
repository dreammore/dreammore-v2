
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:dreammore/helpers/nMHelpers.dart';
import 'package:dreammore/models/Product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class SingleShopItem extends StatefulWidget {

  Product product ;

  SingleShopItem({Product product}){
    this.product = product;
  }
  @override
  State<StatefulWidget> createState() => _SingleShopItemState();
}

class _SingleShopItemState extends State<SingleShopItem>{
  @override
  Widget build(BuildContext context) {

     return Container(
       width: 210,
       height: 235,
       child:Container(
         height: 265,
         child:  Column(
           children: <Widget>[
             Container(
               margin: EdgeInsets.fromLTRB(0, 10, 0, 0),
               width: 210,
               child: Column(
                 children: <Widget>[
               ClipRRect(
                 borderRadius: BorderRadius.circular(4.0),
                 child: CachedNetworkImage(
                   height: 120,
                   width: 200,
                   fit:BoxFit.fill ,
                   imageUrl: widget.product.images[0].src.toString(),
                 ),
               ),

                   Container(
                     decoration: BoxDecoration(
                       image: DecorationImage(
                           image: AssetImage('assets/img/bg/repeatGrid.png'),
                           repeat: ImageRepeat.repeat
                       ),
                     ),
                     padding: EdgeInsets.all(15),
                     child: Column(

                       children: <Widget>[
                         Container(
                           margin: EdgeInsets.fromLTRB(0, 0, 0, 5),
                           child:Align(
                             alignment: Alignment.centerLeft,
                             child: Text(widget.product.name,textAlign: TextAlign.start,style:GoogleFonts.roboto(
                               fontSize: 13,
                               fontWeight: FontWeight.w500,
                               color: Colors.black,
                             ),)
                           ),
                         ),

                        Container(
                          child:     Row(
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              Text("Prix de vente : ${widget.product.price}" ,
                                style:GoogleFonts.workSans(
                                    fontSize: 13,
                                    color: Colors.black,
                                    fontWeight: FontWeight.bold
                                ),),
                              /*aadd to cart  button*/
                              GestureDetector(
                                onTap: (){
                                  print("ds");
                                },
                                child: Container(
                                  decoration: neoShopWhite,
                                  height: 30,
                                  width: 30,
                                  child:  Icon(AntDesign.shoppingcart,size: 15,color: Colors.black,),
                                ),
                              )


                            ],
                          ),
                        )
                       ],
                     ),
                   )


                 ],
               )
             ),

           ],
         ),
       ),
     );
  }

}