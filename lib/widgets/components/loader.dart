

import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';

class LoaderWidget extends StatefulWidget{

  final String asset;
  final double height;

  LoaderWidget({this.asset,this.height});

  @override
  State<StatefulWidget> createState() {
      return _LoaderWidgetState();
  }

}


class _LoaderWidgetState extends State<LoaderWidget> with TickerProviderStateMixin{
  AnimationController _animationController;


  @override
  void initState() {
    super.initState();
    _animationController= AnimationController(vsync: this);
  }


  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      height: widget.height,
            child:Lottie.asset(
             widget.asset
        ),
    );
  }

}