import 'package:cached_network_image/cached_network_image.dart';
import 'package:dreammore/models/RESTResponse/Trends.dart';
import 'package:dreammore/widgets/TrendsNewsItemScreen.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
class TrendsItems extends StatelessWidget{


  final Trend trend;

  const TrendsItems({this.trend}) ;

  @override
  Widget build(BuildContext context) {



    var size = MediaQuery.of(context).size;
    var constraints = new BoxConstraints();

    var _constraintsheight = constraints.maxHeight * .65;

    print("dsdsd ${trend.postId}");


    return Container(
      margin: EdgeInsets.all(15),
      child: Column(
        children: <Widget>[
          Stack(
            children: <Widget>[
              ClipRRect(
                child:CachedNetworkImage(
                    imageUrl:  trend.featuredImage.toString(),
                  fit: BoxFit.cover,
                    height: 220,
                    width:MediaQuery.of(context).size.width
      ),
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
              ),
              GestureDetector(
                onTap: () async{
                  print("${trend.postId}");
                  Navigator.pushNamed(context, 'trendsDetail',arguments:TrendItemArguments(trend.postId));
                },
                onDoubleTap: ()async{
                  print("object double");
                },
                child: Container(
                  width: size.width,
                  height: 192.0,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.all(
                          Radius.circular(8.0)
                      ),
                      gradient: LinearGradient(
                          colors: [
                            Colors.black.withOpacity(0.8),
                            Colors.black.withOpacity(0.0),
                          ],
                          begin: Alignment.topCenter,
                          end: Alignment.bottomCenter,
                          stops: [
                            0.0,
                            0.7
                          ]
                      )
                  ),
                ),
              ),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: EdgeInsets.only(
                        left: 12.0,
                        top: 12.0,
                        right: 20.0
                    ),
                    child: Text(trend.postTitle
                       ,
                      style: GoogleFonts.poppins(
                          color: Colors.white
                      ),
                      overflow: TextOverflow.ellipsis,
                      maxLines: 3,
                    ),
                  )
                ],
              ),
            ],
          ),
          SizedBox(height: 10,),
          Container(
           child: Row(
             crossAxisAlignment: CrossAxisAlignment.start,
             children: <Widget>[
               Row(
                 children: <Widget>[
                   Icon(AntDesign.message1,size: 15,),
                   SizedBox(width: 5,),
                   Text("${trend.postsComment}")
                 ],
               ),
               SizedBox(width: 5,),
               Row(
                 children: <Widget>[
                   Icon(AntDesign.heart,size: 15,color: Colors.red,),
                   SizedBox(width: 5,),
                   Text("45")
                 ],
               ),
               SizedBox(width: 5,),
             ],
           ),
          ),

        ],
      )
    );


  }

}


class TrendItemArguments{
  int id;

  TrendItemArguments(this.id);
}


