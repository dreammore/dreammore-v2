
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:dreammore/helpers/nMHelpers.dart';
import 'package:dreammore/models/Category.dart';
import 'package:dreammore/models/Product.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';

class SingleCategoryItem extends StatefulWidget {

  Category category ;

  SingleCategoryItem({Category category}){
    this.category = category;
  }
  @override
  State<StatefulWidget> createState() => _SingleCategoryItemState();
}


class SingleCategoryItemDetailArgument{
   int id;
   String categoryImageLink;
   String categoryName;

   SingleCategoryItemDetailArgument(
   {this.id, this.categoryImageLink, this.categoryName});
}

class _SingleCategoryItemState extends State<SingleCategoryItem>{


  @override
  Widget build(BuildContext context) {

    return GestureDetector(
      onTap: (){
        print("object to categorydetail ");
        Navigator.pushNamed(context, 'categoryDetail',arguments:SingleCategoryItemDetailArgument(
            id: widget.category.id,
            categoryImageLink: widget.category.image.src,
            categoryName: widget.category.name
        ));
      },
      child:Container(
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(4),
          image: DecorationImage(
              image: NetworkImage(widget.category.image.src),
              repeat: ImageRepeat.noRepeat
          ),
        ),
        margin: EdgeInsets.only(left: 10,top: 10),
        width: 213,
        height: 163,
        child: Align(
          child: Container(
              decoration:  BoxDecoration(
                color: primaryTopGradient.withOpacity(0.8),
                image: DecorationImage(

                    image: AssetImage('assets/img/bg/repeatGrid.png'),
                    repeat: ImageRepeat.repeat
                ),
              ),

            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height/20,
            child:Align(
              child:  Text(widget.category.name,textAlign: TextAlign.center,style:GoogleFonts.roboto(
                fontSize: 20,
                fontWeight: FontWeight.w500,
                color: Colors.white,
              ),),
            )
          ),
        )
      ) ,
    );

  }

}