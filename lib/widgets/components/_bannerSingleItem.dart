
import 'package:cached_network_image/cached_network_image.dart';
import 'package:dreammore/models/GraphQLResponse/Edge.dart';
import 'package:dreammore/models/RESTResponse/Trends.dart';
import 'package:flutter/cupertino.dart';

class BannerSingleItem extends StatelessWidget {

  final Trend banner;

  const BannerSingleItem({Key key, this.banner}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
        width:MediaQuery.of(context).size.width,
        child:CachedNetworkImage(
            imageUrl:banner.featuredImage.toString(),
            fit: BoxFit.fill,
            height: 220,
            width:MediaQuery.of(context).size.width
        ),
    );
   /* height: 200,
    initialPage: 0,
    disableCenter: true,
    enableInfiniteScroll: true,
    reverse: false,
    autoPlay: true,
    autoPlayInterval: Duration(seconds: 4),
    autoPlayAnimationDuration: Duration(milliseconds: 800),
    autoPlayCurve: Curves.fastOutSlowIn,
    aspectRatio: 2.0,
    enlargeCenterPage: true,
    scrollDirection: Axis.horizontal,*/
  }

}