import 'package:cached_network_image/cached_network_image.dart';
import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:dreammore/providers/ProductsProvider.dart';
import 'package:dreammore/widgets/components/_singleCategoryScreenItem.dart';
import 'package:dreammore/widgets/components/loader.dart';
import 'package:dreammore/widgets/components/singleCategoryItem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class SingleCategoryDetailScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
    return _SingleCategoryDetailStateScreen();
  }

}

class _SingleCategoryDetailStateScreen extends State{


  @override
  Widget build(BuildContext context) {
    final SingleCategoryItemDetailArgument args = ModalRoute
        .of(context)
        .settings
        .arguments;

    print("arguments is ${args.id} ");



    return ChangeNotifierProvider<ProductProvider>(
      create: (context)=>ProductProvider(),
       child: Consumer<ProductProvider>(
         builder: (BuildContext context,ProductProvider _productProvider,Widget child){
           if (_productProvider.getCategorySub().length == 0){
             _productProvider.fetchCategorySub(args.id);
             return Scaffold(
               body:Container(
                 decoration: BoxDecoration(
                   color: shopItemImgBg,
                   image: DecorationImage(
                       colorFilter: ColorFilter.mode(shopItemImgBg.withOpacity(0.5), BlendMode.dstATop),
                       image: AssetImage('assets/img/bg/repeatGrid.png'),
                       repeat: ImageRepeat.repeat),
                 ),
                 child: Center(
                     child:Column(
                       mainAxisAlignment: MainAxisAlignment.center,
                       children: <Widget>[
                         Image.asset("assets/img/logo-text.png",height: 50),
                         LoaderWidget(height: 50,asset: 'assets/lotties/loader.json',)
                       ],
                     )
                 ),
               ) ,
             );

           }else{
             return DefaultTabController(
               length: _productProvider.getCategorySub().length,
               child: Scaffold(
                   appBar:PreferredSize(
                     preferredSize: Size.fromHeight(80),
                     child:  new AppBar(
                       backgroundColor: shopItemImgBg,
                       elevation: 0,
                       actions: <Widget>[
                         IconButton(
                           onPressed:(){},
                           icon: Icon(MaterialIcons.search,color: Colors.black,),
                         ),
                         IconButton(
                           onPressed:(){},
                           icon: Icon(AntDesign.shoppingcart,color: Colors.black,),
                         ),
                         IconButton(
                           onPressed:(){},
                           icon: Icon(MaterialCommunityIcons.bell,color: Colors.black,),
                         )
                       ],
                       leading:  IconButton(
                         onPressed:(){
                           Navigator.pop(context);
                         },
                         icon: Icon(Icons.arrow_back,color: Colors.black,),
                       ),
                       centerTitle: false,
                       title: Container(
                         width: 130,
                         child: Row(
                           mainAxisAlignment: MainAxisAlignment.spaceBetween,
                           children: <Widget>[
                             Text(args.categoryName,style: GoogleFonts.playfairDisplay(
                                 fontSize: 18,
                                 color: Colors.black,
                                 fontWeight: FontWeight.bold
                             ),),
                           ],
                         ),
                       ),
                       flexibleSpace:Container(
                           alignment: Alignment.center,
                           decoration:  BoxDecoration(
                             image: DecorationImage(
                                 colorFilter: ColorFilter.mode(shopItemImgBg.withOpacity(0.4), BlendMode.dstATop),
                                 image: AssetImage('assets/img/bg/repeatGrid.png'),
                                 repeat: ImageRepeat.repeat
                             ),
                           ),
                           child: Container()
                       ),
                       bottom: TabBar(
                           indicatorColor: primaryTopGradient,
                           unselectedLabelColor: Colors.grey,
                           labelColor: Colors.black,
                           isScrollable: true,
                           labelStyle: GoogleFonts.workSans(
                               fontSize: 15
                           ),

                           tabs:_productProvider.getCategorySub().map((category){
                             print("category is ${category.name}");
                             return   Container(
                               width: 95,
                               child: Tab(
                                 text: category.name,
                               ),
                             );
                           }).toList()
                       ),
                     ),
                   ),
                   body:  Container(
                       margin: EdgeInsets.only(left: 5,right: 5),
                       decoration:  BoxDecoration(
                         color: shopItemImgBg,
                         image: DecorationImage(
                             colorFilter: ColorFilter.mode(shopItemImgBg.withOpacity(0.4), BlendMode.dstATop),
                             image: AssetImage('assets/img/bg/repeatGrid.png'),
                             repeat: ImageRepeat.repeat
                         ),
                       ),
                       child: TabBarView(
                         children:_productProvider.getCategorySub().map((e) {
                             return SingleCategoryItemScreen(e.id);
                         }).toList()
                       )
                   )
               ),
             );
           }
         },
       ),

    );


  }

}
