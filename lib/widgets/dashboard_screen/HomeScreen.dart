


import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:dreammore/models/RESTResponse/Trends.dart';
import 'package:dreammore/providers/TrendsProvider.dart';
import 'package:dreammore/widgets/components/TrendsNewsItem.dart';
import 'package:dreammore/widgets/components/loader.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class HomeScreen extends StatefulWidget{
  @override
  State<StatefulWidget> createState() {
     return _HomeScreenState();
  }

}


class _HomeScreenState extends State<HomeScreen> with SingleTickerProviderStateMixin{
  var scrollController = ScrollController();
  TabController tabController;



  void initState() {
    super.initState();
    tabController = TabController(length: 3, vsync: this);

  }

  @override
  Widget build(BuildContext context) {
    return Container(
        decoration: BoxDecoration(
          image: DecorationImage(
              image: AssetImage('assets/img/bg/repeatGrid.png'),
              repeat: ImageRepeat.repeat
          ),
        ),
        child: ChangeNotifierProvider<TrendsProvider>(
          create: (context)=>TrendsProvider(),
          child: Consumer<TrendsProvider>(
            builder: (context,provider,child){

              if(provider.trendsList == null){
                provider.fetchAllTrends(context);
                return Container(
                    child: Center(
                        child:Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: <Widget>[
                            Image.asset("assets/img/logo-text.png",height: 50),
                            LoaderWidget(height: 50,asset: 'assets/lotties/loader.json',)
                          ],
                        )
                    ),
                  );


              }else{
                return   Column(
                  children: [
                    Container(
                      color: Colors.black,
                      child: TabBar(
                        controller: tabController,
                        indicatorColor: primaryTopGradient,
                        tabs: [
                          Tab(
                            text: "Populaires",
                          ),
                          Tab(
                            text: "Suivies",
                          ),
                          Tab(
                            text: "Stories",
                          ),
                        ],
                      ),
                    ),
                    Expanded(
                      child:  TabBarView(
                          controller: tabController,
                          children: [
                            ListView.builder(
                                itemCount: provider.trendsList.length,
                                itemBuilder: (BuildContext ctxt, int index) {
                                   Trend trend = provider.trendsList[index];
                                  return TrendsItems(trend:trend);
                                }

                            ),
                            Icon(Icons.movie),
                            Icon(Icons.games),
                          ]),
                    )

                  ],
                );
              }
            },
          ),
        )
    );
  }




}