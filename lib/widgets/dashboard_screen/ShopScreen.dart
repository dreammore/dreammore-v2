import 'dart:collection';

import 'package:cached_network_image/cached_network_image.dart';
import 'package:carousel_pro/carousel_pro.dart';
import 'package:dreammore/helpers/CustomConstants.dart';
import 'package:dreammore/models/RESTResponse/Trends.dart';
import 'package:dreammore/providers/ProductsProvider.dart';
import 'package:dreammore/providers/TrendsProvider.dart';
import 'package:dreammore/widgets/components/_bannerSingleItem.dart';
import 'package:dreammore/widgets/components/_singleShopItem.dart';
import 'package:dreammore/widgets/components/singleCategoryItem.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

class ShopScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() {
    return _ShopScreen();
  }
}

class _ShopScreen extends State<ShopScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
           height: MediaQuery.of(context).size.height,
          decoration: BoxDecoration(
            color: shopItemImgBg,
            image: DecorationImage(
                colorFilter: ColorFilter.mode(shopItemImgBg.withOpacity(0.5), BlendMode.dstATop),
                image: AssetImage('assets/img/bg/repeatGrid.png'),
                repeat: ImageRepeat.repeat),
          ),
          child:  SingleChildScrollView(
              child: Container(
                margin: EdgeInsets.fromLTRB(16, 5, 0, 0),
                child: Column(
                  children: <Widget>[
                    ShopHeader(context),
                    NewestProductsWidget(context),
                    CategoriesProductsWidget(context)
                    /*defined the  category part*/
                  ],
                ),
              ))),
    );
  }




  Widget ShopHeader(BuildContext context){
    return Column(
         crossAxisAlignment: CrossAxisAlignment.start,
         children: <Widget>[
           SizedBox(height: 10,),
           Text("Boutique",style: GoogleFonts.playfairDisplay(
             fontSize: 30,
             fontWeight: FontWeight.bold
           ),),

           Container(
             margin: EdgeInsets.fromLTRB(0, 15, 20, 0),
             child:Text('Obtenez les produits les moins chers pour votre usage quotidien',
                 style:GoogleFonts.roboto(
                   fontSize: 17,
                   fontWeight: FontWeight.w700
                 )
             ) ,
           )

         ],
    );
  }


  Widget ShopCarousel(List banner){

   // return Text("dsdsd");
    return new Container(
      height: 250,
      color: Colors.white,
      child: new Carousel(
        dotSize: 4.0,
        dotSpacing: 15.0,
        autoplayDuration: Duration(milliseconds: 7000),
        dotBgColor: Colors.white.withOpacity(0.5),
        dotColor: Colors.black,
        boxFit: BoxFit.cover,
        images: banner.map((e) => CachedNetworkImage(
             imageUrl: e.featuredImage.toString(),
           )
        ).toList(),
      ),
     /* child: Column(
        children: banner.map((e) => Text("${e.featuredImage}")).toList(),
      ),*/
    );
  }

  //create page widget
   Widget NewestProductsWidget(BuildContext context){
     return Container(
       margin: EdgeInsets.fromLTRB(0, 35, 4, 0),
       height: 275,
       child:  Column(
         children: <Widget>[
           Container(
             child: Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children: <Widget>[
                 Text("Nouvelles offres", style:GoogleFonts.roboto(
                     fontSize: 17,
                     fontWeight: FontWeight.w700
                 ),),


                 GestureDetector(
                   onTap: (){
                     print("object");
                   },
                   child:  Row(
                     children: <Widget>[
                       Text("Voir tout",
                         style:GoogleFonts.roboto(
                             fontSize: 17,
                             fontWeight: FontWeight.w500
                         ),),
                       Container(
                         margin: EdgeInsets.fromLTRB(5, 0, 0, 0),
                         width: 30,
                         height: 2,
                         color: Colors.black,
                       ),
                       Icon(MaterialIcons.keyboard_arrow_right)
                     ],
                   ) ,
                 )

               ],
             ),
           ),
           Flexible(
               child: Consumer<ProductProvider>(
             builder: (context,provider, child) {

               //value.fetchLastInsertedProducts();

               if (provider.getLastInsertedProducts().length == 0){
                 provider.fetchLastInsertedProducts();
                 return Text("data");
               }else{
                 return ListView.builder(
                     scrollDirection: Axis.horizontal,
                     shrinkWrap: true,
                     physics: ScrollPhysics(),
                     itemCount: provider.getLastInsertedProducts().length,
                     itemBuilder: (context,index){
                       return Container(
                         margin: EdgeInsets.fromLTRB(5,5,5,0),
                         child: SingleShopItem(product: provider.getLastInsertedProducts()[index],),
                       );
                     }
                 );
               }
             },))



         ],
       ),
     );

   }
   Widget CategoriesProductsWidget(BuildContext context){
     return Container(
       height: 170,
       child:  Column(
         children: <Widget>[
           Container(
             margin: EdgeInsets.fromLTRB(0, 0, 4, 0),
             child: Row(
               mainAxisAlignment: MainAxisAlignment.spaceBetween,
               children: <Widget>[
                 Text("Par Catégories", style:GoogleFonts.roboto(
                     fontSize: 17,
                     fontWeight: FontWeight.w700
                 ),),
               ],
             ),
           ),
           Flexible(
               child: Consumer<ProductProvider>(
             builder: (context,provider, child) {

               //value.fetchLastInsertedProducts();

               if (provider.getCategory().length == 0){
                 provider.fetchProductsCategories();
                 return Text("data");
               }else{
                 return ListView.builder(
                     scrollDirection: Axis.horizontal,
                     shrinkWrap: true,
                     physics: ScrollPhysics(),
                     itemCount: provider.getCategory().length,
                     itemBuilder: (context,index){

                       return Container(
                         height: 163,
                         margin: EdgeInsets.fromLTRB(5,5,5,0),
                         child: SingleCategoryItem(category:provider.getCategory()[index]),
                       );
                     }
                 );
               }
             },))



         ],
       ),
     );

   }
}
