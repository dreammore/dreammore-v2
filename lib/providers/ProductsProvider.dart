


import 'dart:convert';

import 'package:dreammore/helpers/ApiClient.dart';
import 'package:dreammore/models/Category.dart';
import 'package:dreammore/models/Product.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:provider/provider.dart';

class ProductProvider extends ChangeNotifier {
  
  //init the client 
  //set the state 
  //create getters and setters for the projects 
  
  //set up the state 
  
  List <Product> _products = [];

  bool productLoaded = false;
  int isLoadingStatus = 0;/*
   0 end loading
   1 is loading

  */

  List <Product> _categoryProducts = [];

  List <Category> _categories = [];

  List <Category> _subCategory = [];

  Product _product;
  
  ApiClient _apiClient ;

  ProductProvider(){
     this._apiClient = new ApiClient();
  }
  
  
  Future fetchLastInsertedProducts(){

    var productsList;
    List parsedResponse;
    print("fetch");
    
    this._apiClient.getAllProducts().then((response) => {
        if(response.statusCode == 200 || response.statusCode ==201){
          productsList = json.decode(response.body).cast<Map<String, dynamic>>(),
          parsedResponse = productsList.map<Product>((product)=>Product.fromJson(product)).toList(),
          setLastInsertedProducts(parsedResponse)
          
        }else{
          print("response is ${response.body.toString()}")
        }
    });
  }

  Future<void> fetchProductsCategories(){
    var  categories;
    List parsedResponse;
    print("fetch");
    this._apiClient.fetchAllProductsCategories().then((response) => {
         if(response.statusCode == 200){
           categories = jsonDecode(response.body).cast<Map<String,dynamic>>(),
           parsedResponse = categories.map<Category>((category)=>Category.fromJson(category)).toList(),
           this.setCategories(parsedResponse)
         }
    });

  }

  Future fetchCategorySub(parentId){
    var _tempData;
    List parsedResponse;
    print("fetch sub category from $parentId");

    this._apiClient.fetchCategorySub(parentId).then((response) {
       if(response.statusCode == 200 ){
          _tempData = jsonDecode(response.body).cast<Map<String,dynamic>>();
          parsedResponse = _tempData.map<Category>((_categories)=>Category.fromJson(_categories)).toList();
          this.setSubCategories(parsedResponse);
       }

    });

  }



  Future fetchSingleCategoryProducts({int categoryId, BuildContext context}){
    List _decodedResponse ;
    List parsedProductResponse;

    this._apiClient.fetchCategoryProducts(categoryId: categoryId).then((response) {
      if (response.statusCode ==200){
        _decodedResponse = jsonDecode(response.body).cast<Map<String,dynamic>>();
        parsedProductResponse = _decodedResponse.map<Product>((product) => Product.fromJson(product)).toList();
        this.setCategoryProducts(value: parsedProductResponse,isloaded: true);
      }else{

      }

    });
  }

  Future<void>  fetchSingleProductsDetails({int productId}) async {
      var _decodedResponse;
      Future.delayed(Duration.zero, () async {
        setLoadingStatus(value: 1);
      });

      this._apiClient.fetchSingleProductDetails(productId: productId).then((response){

        Future.delayed(Duration.zero, () async {
          setLoadingStatus(value: 0);
        });

          if (response.statusCode == 200){
            _decodedResponse = jsonDecode(response.body);
             setProduct(Product.fromJson(_decodedResponse));

          }else{
              print ("erreur");
          }

      });
  }


  //setters part


  void setCategoryProducts({value,isloaded}){
    _categoryProducts = value;
    this.productLoaded = isloaded;
    notifyListeners();
  }



  void setLastInsertedProducts(value){
      _products = value;
      notifyListeners();
  }


  void setCategories(value){
    _categories = value;
    notifyListeners();
  }


  void setSubCategories(value){
    _subCategory = value;
    notifyListeners();
  }

  void setLoadingState({value}){
    this.isLoadingStatus = value;
    notifyListeners();
  }
  void setLoadingStatus({value}){
    this.isLoadingStatus = value;
    notifyListeners();
  }

  void setProduct(value){
    this._product = value;
    notifyListeners();
  }


  getLoadingStatus(){
    return this.isLoadingStatus;
  }
  getLoadingState(){
    return this.productLoaded;
  }

  List <Category> getCategory(){
    return this._categories;
  }

  List <Category> getCategorySub(){
    return this._subCategory;
  }
  
  List<Product> getLastInsertedProducts(){
    return this._products;
  }

  List <Product> getCategoriesProducts(){

    return this._categoryProducts;
  }

  Product getProduct(){
    return this._product;
  }



  @override
  void dispose() {
    this.setCategoryProducts();
    this.setLoadingState();
    super.dispose();

  }
}