import 'dart:convert';

import 'package:dreammore/helpers/ApiClient.dart';
import 'package:dreammore/models/RESTResponse/DashboardResponse.dart';
import 'package:dreammore/models/RESTResponse/Trends.dart';
import 'package:dreammore/models/SingleTrend.dart';
import 'package:flutter/cupertino.dart';
import 'package:global_configuration/global_configuration.dart';

class TrendsProvider extends ChangeNotifier {

  List<Trend> trendsList;
  List<Trend> shopBanners;
  SingleTrend trend;
  bool loading = false;
  ApiClient _apiClient = ApiClient();



  Future fetchAllTrends(context){

    List trendsList;
    var trends ;


    _apiClient.fetchAllTrends().then((data) => {

       if(data.statusCode == 200){

         trends = json.decode(data.body).cast<Map<String, dynamic>>(),
         print("dsd${data.body}"),
         trendsList =  trends.map<Trend>((trend)=>Trend.fromJson(trend)).toList(),
         setTrendsList(trendsList)

         //print(trendsList)
       }
    });

  }


  Future fetchSingleTrendById(int id){


    _apiClient.fetchSingleTrendById(id).then((response) =>{

     print("response ${response.body.toString()}"),
       if(response.statusCode == 200){
         setTrend(SingleTrend.fromJson(
           json.decode(response.body)
         ))
       }else{
         print("${response.statusCode}")
       }
    });
  }


  Future fetchAllShopBanner(context){
    var bannerListList;
    List _shopBanners ;
    _apiClient.fetchAllShopBanner().then((response) =>{
       if(response.statusCode == 200){
         bannerListList = json.decode(response.body).cast<Map<String, dynamic>>(),
         _shopBanners =  bannerListList.map<Trend>((trend)=>Trend.fromJson(trend)).toList(),
         setShopBanner(_shopBanners),
       }
    });
  }

  void setLoading(value){
    loading = value;
    notifyListeners();
  }

  void setTrendsList(_trendsList){
    trendsList = _trendsList;
    notifyListeners();
  }

  void setShopBanner(_bannersList){
    shopBanners = _bannersList;
    notifyListeners();
  }


  void setTrend(_trend){
    trend = _trend;
    notifyListeners();
  }

   getAllTrends(){
    return this.trendsList;
  }
  

}