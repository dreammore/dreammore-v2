import 'dart:convert';

import 'package:dreammore/helpers/ApiClient.dart';
import 'package:dreammore/helpers/Session.dart';
import 'package:dreammore/models/Customer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:http/http.dart' as Http;

class AuthProvider with ChangeNotifier {
  bool previousLoggedWithGoogle = false;
  String _userIdToken;
  int userId;
  bool isAuthenticated = false;

  bool isProcessing = false;
  ApiClient _apiClient = ApiClient();
  bool previousRegister = false;
  String registrationProcessCode;

  final GoogleSignIn _googleSignIn = GoogleSignIn(scopes: ['email']);
  GoogleSignInAccount _googleSignInAccount;



  @override
  void dispose() {
    super.dispose();
  }

  Future signInWithGoogle() async {
    try {
      this.isRegisteringProcessing(true);
      _googleSignInAccount = await _googleSignIn.signIn();
      var authenticationResponse = await _googleSignInAccount.authentication;
      var idToken = authenticationResponse.idToken;
      setUserIdToken(idToken);
    } catch (error) {
      print("error is " + error.toString());
    }
  }

  Future sendDataToServer(String token) async {
    var decodedresponse;

    var registeringResponse = await _apiClient.registerUser(idToken: token);
    var decodedRegisteringResponse = jsonDecode(registeringResponse.body);
    this.setRegistrationProcessCode(decodedRegisteringResponse['code']);

    if (decodedRegisteringResponse['code'] == 'USER_FOUND') {
      print("send to server :user found ");
      this.setIfPreviousRegistered(true);
      this.setUserId(decodedRegisteringResponse['user']['ID']);
      this.isRegisteringProcessing(false);
    } else {
      print("send to server :user not found ");
      this.setIfPreviousRegistered(false);
      this.setUserId(decodedresponse['user']['ID']);
      this.isRegisteringProcessing(false);
    }
  }

  void setLogged(value,{userId:int}) {
    this.isAuthenticated = value;
    Session.setBool('isAuthenticated', this.isAuthenticated);
    Session.setInt('user_id', userId);
    notifyListeners();
  }

  checkIfLogged() async {
    this.isAuthenticated = await Session.getBool("isAuthenticated");
    return this.isAuthenticated;
  }

  void signOut() {
    this.isAuthenticated = false;
    Session.setBool('isAuthenticated', this.isAuthenticated);
    Session.setInt("user_id", 0);
    notifyListeners();
  }

  void setLoggedWithGoogle(value) {
    previousLoggedWithGoogle = value;
    notifyListeners();
  }

  void setUserIdToken(value) {
    _userIdToken = value;
    notifyListeners();
  }

  String getUserIdToken() {
    return this._userIdToken;
  }

  void setIfPreviousRegistered(value) {
    previousRegister = value;
    notifyListeners();
  }

  void setUserId(value) {
    userId = value;
    notifyListeners();
  }

  getUserId() {
    return userId;
  }

  getIfPreviousRegistred() {
    return this.previousRegister;
  }

  void isRegisteringProcessing(value) {
    this.isProcessing = value;
    notifyListeners();
  }

  void setRegistrationProcessCode(value){
    this.registrationProcessCode = value;
    notifyListeners();
  }

  getRegistrationProcessCode(){
    return this.registrationProcessCode;
  }

  getIfisRegisteringProcess() {
    return this.isProcessing;
  }
}
