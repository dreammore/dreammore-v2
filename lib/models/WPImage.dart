
class WPImage{

  int _id;
  String _date_created;
  String _date_created_gmt;
  String _date_modified;
  String _date_modified_gmt;
  String _src;
  String _name;

  WPImage(this._id, this._date_created, this._date_created_gmt,
      this._date_modified, this._date_modified_gmt, this._src, this._name);




  WPImage.name(this._src);


  factory WPImage.fromJsonName(Map<String,dynamic>json){


   return json == null? WPImage.name('http://localhost:8001/wp-content/uploads/2020/10/no-camera.png'): WPImage.name(json['src'].toString());



  }




  factory WPImage.fromJson(Map<String,dynamic>json)=> WPImage(
      json['id'],
      json['date_created'],
      json['date_created_gmt'],
    json['date_modified'],
    json['date_modified_gmt'],
      json['src'],
      json['name'],
    );

  String get name => _name;

  String get src => _src;

  String get date_modified_gmt => _date_modified_gmt;

  String get date_modified => _date_modified;

  String get date_created_gmt => _date_created_gmt;

  String get date_created => _date_created;

  int get id => _id;
}