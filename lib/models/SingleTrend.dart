class SingleTrend {
  int postId;
  String postTitle;
  String postExcerpt;
  Object featuredImage;
  int postsComment;
  String postContent;

  SingleTrend({this.postId, this.postTitle, this.postExcerpt, this.featuredImage,
    this.postsComment,this.postContent});

  factory SingleTrend.fromJson(Map<String, dynamic> json) => SingleTrend(
    postId: json["ID"],
    postTitle: json["post_title"],
    postExcerpt: json["post_excerpt"],
    featuredImage: json["image"],
    postsComment: json["comment_count"],
    postContent: json["post_content"],
  );

  Map<String, dynamic> toJson() => {
    "post_id": postId,
    "post_title": postTitle,
    "post_excerpt": postExcerpt,
    "featured_image": featuredImage,
    "posts_comment": postsComment,
    "post_content":postContent
  };
}
