
import 'package:dreammore/models/WPImage.dart';

class Category {

  int _id;
  String _name;
  int _count;
  WPImage _image;

  Category(this._id, this._name, this._count, this._image);


  WPImage get image => _image;

  set image(WPImage value) {
    _image = value;
  }

  int get count => _count;

  String get name => _name;

  int get id => _id;




  factory Category.fromJson(Map<String,dynamic>jsonResponse)=> Category(
        jsonResponse['id'],
        jsonResponse['name'],
        jsonResponse['count'],
        WPImage.fromJsonName(jsonResponse["image"]));


}


