import 'package:dreammore/models/WPImage.dart';

class Product {
  int _id;
  String _name;
  String _price;
  List <WPImage> _images;
  String _description;

  Product(this._id, this._name, this._price, this._images,this._description);


  factory Product.fromJson(Map<String, dynamic> json)=>
      Product(json['id'], json['name'], json['price'],
          List<WPImage>.from(json["images"].map((x) => WPImage.fromJson(x))),
         json['description']
      );

  List<WPImage> get images => _images;

  String get price => _price;

  String get name => _name;

  int get id => _id;

  String get description => _description;
}