

import 'dart:convert';

import 'package:dreammore/models/GraphQLResponse/Node.dart';

class Edge {

 final Node nodes;

 Edge({this.nodes});

 factory Edge.fromJson(Map <String, dynamic> json){
   return Edge(
    nodes: Node.fromJson(json["node"])
   );
 }
 Map<String, dynamic> toJson()=>{
   'nodes':this.nodes,
 };
}