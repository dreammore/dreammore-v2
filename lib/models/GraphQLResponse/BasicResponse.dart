
import 'package:dreammore/models/GraphQLResponse/Data.dart';

class BasicResponse {

  final Data data;

  BasicResponse({this.data});

  factory  BasicResponse.fromJson(Map<String,dynamic> _response){
    return BasicResponse(
      data: _response['data']
    );
  }

  Map<String,dynamic> toJson()=>{
    "data":this.data.toJson()
  };
}