
import 'package:dreammore/models/GraphQLResponse/Categories.dart';

class Data {
  final Categories categories;

  Data({this.categories});

  factory Data.fromJson(Map<String, dynamic> json){
    return Data(
      categories: json["categories"]
    );
  }

  Map<String,dynamic> toJson()=>{
    "categories":this.categories.toJson()
  };
}