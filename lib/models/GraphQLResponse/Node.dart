
class Node {
  final int id;
  final String name;

  Node({this.id,this.name});


  factory Node.fromJson(Map<String, dynamic> json){
    return Node(
        id : json['id'],
        name : json['name']
    );
  }


  Map<String, dynamic> toJson()=>{
    'id':id,
    'name':name
  };
}