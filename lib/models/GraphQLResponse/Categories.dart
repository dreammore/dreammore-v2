
import 'package:dreammore/models/GraphQLResponse/Edge.dart';


class Categories {

  final List<Edge>edges;

  Categories({this.edges});

  factory Categories.fromJson(Map<String ,dynamic> json){
    return Categories(
        edges: List<Edge>.from(json["edges"].map((x)=>Edge.fromJson(x))),
    );

  }

  Map<String, dynamic> toJson()=>{
    "edges":List<dynamic>.from(edges.map((e) => e.toJson()))
  };
}