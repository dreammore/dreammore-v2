
class Trend {
  int postId;
  String postTitle;
  String postExcerpt;
  Object featuredImage;
  int postsComment;

  Trend({this.postId, this.postTitle, this.postExcerpt, this.featuredImage,
      this.postsComment});

  factory Trend.fromJson(Map<String, dynamic> json) => Trend(
    postId: json["ID"],
    postTitle: json["post_title"],
    postExcerpt: json["post_excerpt"],
    featuredImage: json["image"],
    postsComment: json["comment_count"],
  );

  Map<String, dynamic> toJson() => {
    "post_id": postId,
    "post_title": postTitle,
    "post_excerpt": postExcerpt,
    "featured_image": featuredImage,
    "posts_comment": postsComment,
  };
}