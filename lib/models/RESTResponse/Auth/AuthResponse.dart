

import 'package:dreammore/models/RESTResponse/Auth/Data.dart';

class AuthResponse {

  final int code;
  final String message;
  final int userId;
  final String exception;

  AuthResponse({this.code,this.message,this.userId,this.exception});

  factory AuthResponse.fromJson(Map<String,dynamic> _response){
    return AuthResponse(
      code: _response['code'],
      message: _response['message'],
      userId: _response['user_id'],
      exception: _response['exception'],
    );
  }

  Map<String,dynamic> toJson()=>{
    "code":this.code,
    "message":this.message,
    "userId":this.userId,
    "exception":this.exception
  };

}