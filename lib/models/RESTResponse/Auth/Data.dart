

class Data {
  final String message;
  final int user_id;
  final String error_code;

  Data({this.message,this.user_id,this.error_code});


  factory Data.fromJson(Map<String,dynamic> _response){
    return Data(
      message: _response['message'],
      user_id: _response['user_id'],
      error_code: _response['error_code']
    );
  }




}