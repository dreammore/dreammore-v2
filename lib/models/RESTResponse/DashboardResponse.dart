import 'package:dreammore/models/RESTResponse/Trends.dart';

class DashboardResponse{
  List<Trend> trends;

  DashboardResponse({this.trends});

  factory DashboardResponse.fromJson(Map<String, dynamic> json) => DashboardResponse(
    trends: List<Trend>.from(json["posts"].map((x) => Trend.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "posts": List<dynamic>.from(trends.map((x) => x.toJson())),
  };
}