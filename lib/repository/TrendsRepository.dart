
import 'package:dreammore/models/RESTResponse/DashboardResponse.dart';
import 'package:global_configuration/global_configuration.dart';
import 'package:http/http.dart' as http;
import 'dart:convert';


class TrendsRepository{

  Future<DashboardResponse> fetchTrendsList() async{
    print("entered");
    final response = await http.get(GlobalConfiguration().getValue("PROD_ENV_URL")+'/trends');
    
    return DashboardResponse.fromJson(json.decode(response.body));
  }

}